var app = angular.module("orders-dashboard-app", []);
var controllers = {};

controllers.dashboardCtrl = function ($scope, $http) {
    $scope.customer = {};
    $scope.productOrder = {};
    $scope.orderId = {};
    $scope.tasks = [];
    $scope.jeopardies = [];
    $scope.filter = "";


    $http.get("/json/sales-order.json")
        .then(function (response) {
            $scope.orders = response.data;
            console.log("Orders", $scope.orders);
        });

    $http.get("/json/ifm-employees.json")
        .then(function (response) {
            $scope.employees = response.data;
            console.log("Employees", $scope.employees);
        });

    $scope.setCustomer = function (customer) {
        console.log("Setting customer", customer)
        $scope.customer = customer;
    };
    
    $scope.setProductOrder = function (productOrder) {
        console.log("Setting ProductOrder", productOrder)
        $scope.productOrder = productOrder;
    };
    
    $scope.setOrderId = function (orderId) {
        console.log("Setting orderId", orderId)
        $scope.orderId = orderId
    };

    $scope.setTasks = function (tasks) {
        console.log("Setting tasks", tasks)
        $scope.tasks = (tasks != null) ? tasks : [];
    };

    $scope.setJeopardies = function (jeopardies) {
        console.log("Setting Jeopardies", jeopardies)
        $scope.jeopardies = (jeopardies != null) ? jeopardies : [];
    };
};

app.controller(controllers);